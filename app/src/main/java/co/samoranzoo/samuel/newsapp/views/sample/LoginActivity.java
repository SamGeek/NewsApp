package co.samoranzoo.samuel.newsapp.views.sample;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.TextView;

import co.samoranzoo.samuel.newsapp.R;
import co.samoranzoo.samuel.newsapp.data.models.User;
import co.samoranzoo.samuel.newsapp.event.NetworkError;
import co.samoranzoo.samuel.newsapp.presenters.sample.LoginPresenter;
import co.samoranzoo.samuel.newsapp.views.BaseActivity;

public class LoginActivity extends BaseActivity<LoginPresenter> implements LoginView, View.OnClickListener {

    private TextInputEditText ed_email;
    private TextInputEditText ed_password;
    private ProgressDialog progressDialog;
    private AppCompatButton loginBtn;
    private TextView textView;

    public static Intent createIntent(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new LoginPresenter(this);

//        loginBtn = (AppCompatButton) findViewById(R.id.btn);
//        loginBtn.setOnClickListener(this);
    }


//    public String getEmail() {
//        if (null == ed_email) ed_email = (TextInputEditText) findViewById(R.id.email);
//        return TextUtils.getString(ed_email);
//    }

//    public String getPassword() {
//        if (null == ed_password) ed_password = (TextInputEditText) findViewById(R.id.password);
//        return TextUtils.getString(ed_password);
//    }

    @Override
    public void showProgress() {

        if (null == progressDialog) {
            progressDialog = new ProgressDialog(this);
//            progressDialog.setMessage(getResources().getString(R.string.loading_dialog_msg));
        }
        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }

    }

    @Override
    public void hideProgress() {
        if (null != progressDialog && progressDialog.isShowing()) progressDialog.dismiss();

    }


    @Override
    public void showUser(User user) {
//        if (null == textView) {
//            textView = (TextView) findViewById(R.id.textDisplayer);
//        }
//
//        textView.setText(new StringBuilder(user.getFirstName()).append("\n")
//                .append(user.getLastName()));
    }

    @Override
    public void throwError(NetworkError error) {

//        if (null == textView) {
//            textView = (TextView) findViewById(R.id.textDisplayer);
//        }
//
//        textView.setText(error.getMessage());

    }

    @Override
    public void onClick(View view) {

//        switch (view.getId()) {
//            case R.id.btn:
//                presenter.authUser(getEmail(), getPassword());
//                break;
//            default:
//                break;
//        }
    }
}
