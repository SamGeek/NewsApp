package co.samoranzoo.samuel.newsapp;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import co.samoranzoo.samuel.newsapp.data.models.News;
import co.samoranzoo.samuel.newsapp.event.NetworkError;
import co.samoranzoo.samuel.newsapp.presenters.news.NewsPresenter;
import co.samoranzoo.samuel.newsapp.utils.Functions;
import co.samoranzoo.samuel.newsapp.views.BaseActivity;
import co.samoranzoo.samuel.newsapp.views.NewsAdapter;
import co.samoranzoo.samuel.newsapp.views.news.NewsView;

public class MainActivity extends BaseActivity<NewsPresenter> implements NewsView {

    private RecyclerView recyclerView;
    private NewsAdapter mAdapter;
    private static List<News> newsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.mainRecycler);
        newsList = new ArrayList<>();
        newsList.add(new News("Tets","Contenu","02/10/2017","totototo"));
        newsList.add(new News("Tets","Contenu","02/10/2017","totototo"));
        newsList.add(new News("Tets","Contenu","02/10/2017","totototo"));
        newsList.add(new News("Tets","Contenu","02/10/2017","totototo"));
        newsList.add(new News("Tets","Contenu","02/10/2017","totototo"));
        newsList.add(new News("Tets","Contenu","02/10/2017","totototo"));

        mAdapter = new NewsAdapter(newsList, MainActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


        presenter =  new NewsPresenter(this);
        presenter.getRemoteNews();

    }

    @Override
    public void showProgress() {

        Functions.showProgress(this);

    }

    @Override
    public void hideProgress() {

        Functions.hideProgress();

    }

    @Override
    public void showNewsList(List<News> newsList) {

        for (News news : newsList){
            newsList.add(news);
            mAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void throwError(NetworkError error) {

    }
}
