package co.samoranzoo.samuel.newsapp.app;

import android.app.Application;
import android.content.Context;

import org.greenrobot.eventbus.EventBus;

import co.samoranzoo.samuel.newsapp.data.service.local.MyRealmMigration;
import co.samoranzoo.samuel.newsapp.data.service.remote.RESTServiceInterface;
import co.samoranzoo.samuel.newsapp.utils.ReadOnly;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Shadaï ALI on 25/11/16.
 * Best Minimalist android project Structure
 * Application singleton class.
 * provide single instance object
 * - Database instance
 * - RESTFULL Service helper instance
 * - Bus helper
 */

public class App extends Application {

    private static final String REST_END_POINT = "http://5.189.178.210:3000/";
    private static EventBus dataBus;
    private static EventBus networkErrorBus;
    private static App instance;
    private static Realm realm;
    // Must be bumped when the schema changes
    private static int DATABASE_VERSION = 0;
    private static EventBus defaultErrorBus;
    private static Retrofit retrofit;
    private static RESTServiceInterface networkService;


    /**
     * Get Instance of application singleon
     * @return App
     */
    @ReadOnly
    public static App getInstance() {
        return instance;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        //lazy ap instanciation
        synchronized (this) {
            if (instance == null) instance = this;
        }

        synchronized (this) {
            if (realm == null) {

                //init realm with the application context
                Realm.init(getContext());

                RealmConfiguration config = new RealmConfiguration.Builder()
                        .schemaVersion(DATABASE_VERSION)
                        //.inMemory() remove this comment if you want in memory database
                        .migration(new MyRealmMigration())
                        .build();
                //realm.setAutoRefresh(true);

                Realm.setDefaultConfiguration(config);

                realm = Realm.getDefaultInstance();

            }
        }

    }

    /**
     * getUser Instance of configurated realm database
     *
     * @return Realm
     */
    @ReadOnly
    public static Realm getRealm() {
        return realm;
    }

    /**
     * Get context of application
     *
     * @return Context
     */
    @ReadOnly
    public static Context getContext() {
        return getInstance().getApplicationContext();
    }

    /**
     * Bus for data transfer in whole App
     * @return EventBus
     */
    //hanna.bennani@gmail.com
    @ReadOnly
    public static EventBus getDataBus() {
        if (null == dataBus) {
            synchronized (App.class) {
                dataBus = EventBus.builder().build();
            }
        }
        return dataBus;
    }

    /**
     * Bus for networkError (HTTP Error)
     *
     * @return EventBus
     */
    @ReadOnly
    public static EventBus getNetworkErrorBus() {
        if (null == networkErrorBus) {
            synchronized (App.class) {
                networkErrorBus = EventBus.builder().build();
            }
        }
        return networkErrorBus;
    }


    /**
     * Bus for All uncategorized error
     *
     * @return EventBus
     */
    @ReadOnly
    public static EventBus getDefaultErrorBus() {
        if (null == defaultErrorBus) {
            synchronized (App.class) {
                defaultErrorBus = EventBus.builder().build();
            }
        }
        return defaultErrorBus;
    }

    @ReadOnly
    public static Retrofit getRetrofit() {
        synchronized (App.class) {
         if (retrofit == null){
             retrofit = new Retrofit.Builder().baseUrl(REST_END_POINT)
                     .addConverterFactory(GsonConverterFactory.create())
                     .build();
         }
            return retrofit;
        }
    }

    @ReadOnly
    public static RESTServiceInterface getNetworkService() {
        synchronized (App.class) {
         if (networkService == null){
             networkService = getRetrofit().create(RESTServiceInterface.class);
         }
            return networkService;
        }
    }
}
