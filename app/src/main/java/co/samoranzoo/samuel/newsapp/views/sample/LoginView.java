package co.samoranzoo.samuel.newsapp.views.sample;

import co.samoranzoo.samuel.newsapp.data.models.User;
import co.samoranzoo.samuel.newsapp.event.NetworkError;

/**
 * Created by Shadaï ALI on 07/12/16.
 *
 * @Doc $doc
 */

public interface LoginView {

     void showProgress();
    void hideProgress();
     void showUser(User user);
    void throwError(NetworkError error);
}
