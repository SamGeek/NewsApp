package co.samoranzoo.samuel.newsapp.views;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import co.samoranzoo.samuel.newsapp.R;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
    }
}
