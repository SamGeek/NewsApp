package co.samoranzoo.samuel.newsapp.event;

import co.samoranzoo.samuel.newsapp.utils.ReadOnly;

/**
 * Created by Shadaï ALI on 26/11/16.
 *
 * Manage all database request Error
 */

@ReadOnly
public class DbMessage {

    public static final int NO_DATA = 1000;
    public static final int TABLE_NOT_FOUND = 1001;
    public static final int DUPLICATE = 1002;
    public static final int UNKNOW = 1003;
    /**Also called when data is updated**/
    public static final int CREATED = 1004;
    public static final int DELETED = 1005;


    private String message = null;
    private int error = 0;

    public DbMessage(int error) {
        this.error = error;
    }

    public int getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }
}
