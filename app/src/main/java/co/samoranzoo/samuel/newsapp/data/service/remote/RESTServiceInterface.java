package co.samoranzoo.samuel.newsapp.data.service.remote;

import java.util.List;

import co.samoranzoo.samuel.newsapp.data.models.News;
import co.samoranzoo.samuel.newsapp.data.models.User;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Shadaï ALI on 27/11/16.
 * <p>
 * <p>Define your Remote REST service</p>
 */

public interface RESTServiceInterface {



    /**
     * You must define your web serices here @see @link(https://square.github.io/retrofit/)
     **/
    @GET("/user")
    Call<List<User>> getUser();

    // interface de recuperation de la liste des news
    @GET("/news")
    Call<List<News>> getNews();

}
