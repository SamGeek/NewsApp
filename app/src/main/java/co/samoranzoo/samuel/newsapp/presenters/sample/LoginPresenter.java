package co.samoranzoo.samuel.newsapp.presenters.sample;

import org.greenrobot.eventbus.Subscribe;

import co.samoranzoo.samuel.newsapp.data.models.User;
import co.samoranzoo.samuel.newsapp.data.service.DataManager;
import co.samoranzoo.samuel.newsapp.event.NetworkError;
import co.samoranzoo.samuel.newsapp.presenters.BasePresenter;
import co.samoranzoo.samuel.newsapp.views.sample.LoginView;

/**
 * Created by Shadaï ALI on 08/12/16.
 */

public class LoginPresenter extends BasePresenter{

    private LoginView view;

    public LoginPresenter(LoginView view) {
        super();
        this.view = view;
    }

    public void authUser(String email, String password) {
        view.showProgress();
        DataManager.getUser(DataManager.NRFRS);
    }

    @Subscribe
    public void onUser(User user) {
        view.hideProgress();
        view.showUser(user);
    }

    @Subscribe
    public void onError(NetworkError error) {
        view.hideProgress();
        view.throwError(error);

    }
}
