package co.samoranzoo.samuel.newsapp.data.service.local;

import android.support.annotation.NonNull;

import org.greenrobot.eventbus.EventBus;

import co.samoranzoo.samuel.newsapp.app.App;
import co.samoranzoo.samuel.newsapp.event.DbMessage;
import co.samoranzoo.samuel.newsapp.utils.ReadOnly;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;


/**
 * Created by Shadaï ALI on 25/11/16.
 * <p>
 * Manage local dataBase helper
 * provides some features like
 * - save any defined model (@see data.models) asynchronously
 * - retrieve specified data on local and push it on high layers via databus
 * - delete or update data
 */

public class DataBaseService {

    private Realm realm;

    @ReadOnly
    public DataBaseService() {
        if (realm == null) {
            realm = App.getRealm();
        }
    }


    /**
     * Save data in local storage
     *
     * @param data object that you want persist
     */
    @ReadOnly
    public void save(@NonNull final RealmObject data) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(data);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                EventBus.getDefault().post(new DbMessage(DbMessage.CREATED));
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                EventBus.getDefault().post(new DbMessage(DbMessage.DELETED));
            }
        });
    }


    /**
     * @param objectClass class of requested data model i.e <code>User.class</code>
     * @param <T>         requested object class i.e : User.class
     * @return RealmResults List of finded data
     */
    @ReadOnly
    public <T extends RealmObject> void findAll(final Class<T> objectClass) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults rs = realm.where(objectClass).findAll();
                if (!rs.isEmpty())
                    App.getDataBus().post(rs);
                else //push No data error to all subscriber
                    App.getDefaultErrorBus().post(new DbMessage(DbMessage.NO_DATA));
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                //TODO : throw correct error
                App.getDefaultErrorBus().post(new DbMessage(DbMessage.UNKNOW));
            }
        });
    }


    /**
     * Execute Realm Query for fecth data and push it on the bus
     * @param query Realm query for fetch data
     * @param <T> in which model the query is based
     */
    @ReadOnly
    public <T extends RealmObject> void exec(final RealmQuery<T> query) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                RealmResults rs = query.findAll();
                if (!rs.isEmpty())
                    App.getDataBus().post(rs);
                else //push No data error to all subscriber
                    App.getDefaultErrorBus().post(new DbMessage(DbMessage.NO_DATA));
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                //TODO : throw correct error
                App.getDefaultErrorBus().post(new DbMessage(DbMessage.UNKNOW));
            }
        });
    }

}
