package co.samoranzoo.samuel.newsapp.views.news;

import java.util.List;

import co.samoranzoo.samuel.newsapp.data.models.News;
import co.samoranzoo.samuel.newsapp.event.NetworkError;

/**
 * Created by Shadaï ALI on 07/12/16.
 *
 * @Doc $doc
 */

public interface NewsView {

     void showProgress();
    void hideProgress();
     void showNewsList(List<News> newsList);
    void throwError(NetworkError error);
}
