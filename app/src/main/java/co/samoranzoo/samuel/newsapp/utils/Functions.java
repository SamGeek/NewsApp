package co.samoranzoo.samuel.newsapp.utils;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by samuel on 11/06/17.
 */

public class Functions {


    private static ProgressDialog load;

    public static void showProgress(Context mContext) {

        load = new ProgressDialog(mContext);
        if (null == load) {
            load = new ProgressDialog(mContext);
        }

        load.show();
    }

    public static void hideProgress() {
        if (null != load ) load.dismiss();
        load = null;

    }

}
