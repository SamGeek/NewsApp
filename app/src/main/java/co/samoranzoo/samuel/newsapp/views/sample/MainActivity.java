package co.samoranzoo.samuel.newsapp.views.sample;

import android.os.Bundle;

import org.greenrobot.eventbus.Subscribe;

import co.samoranzoo.samuel.newsapp.R;
import co.samoranzoo.samuel.newsapp.data.models.User;
import co.samoranzoo.samuel.newsapp.data.service.DataManager;
import co.samoranzoo.samuel.newsapp.event.NetworkError;
import co.samoranzoo.samuel.newsapp.views.BaseActivity;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DataManager.getUser(DataManager.PERSISTED_NRFRS);
    }

    @Subscribe
    public void showUser(User user){
//        ((TextView) findViewById(R.id.textView)).setText(user.getFirstName()
//        + "\n" + user.getLastName());
    }

    @Subscribe
    public void onError(NetworkError networkError){
//        ((TextView) findViewById(R.id.textView)).setText(networkError.getMessage());
    }
}
