package co.samoranzoo.samuel.newsapp.data.service;

import co.samoranzoo.samuel.newsapp.data.models.User;
import co.samoranzoo.samuel.newsapp.data.service.local.DataBaseService;
import co.samoranzoo.samuel.newsapp.data.service.remote.RESTServices;
import co.samoranzoo.samuel.newsapp.utils.ReadOnly;
import io.realm.RealmObject;

/**
 * Created by Shadaï ALI on 04/12/16.
 *
 * @Doc $doc
 */

public class DataManager {

    private static  RESTServices restServices = new RESTServices();
    private static DataBaseService dataBaseService = new DataBaseService();

    /**if you need data manger refresh data from remote source*/
    //NEED_REFRESH_FROM_REMOTE_SOURCE
    public static final int NRFRS = 0;

    /**
    if you want persisted data if available only
     */
    public static final int PERSISTED_DATA_ONLY = 1;

    /**if you want persisted data immediatly and run remote source fecth*/
    public static final int PERSISTED_NRFRS = 10;


    public static  void getUser(final int whichDataYouNeed){
        switch (whichDataYouNeed){
            case NRFRS:
                restServices.getRemoteUser();
                break;
            case PERSISTED_DATA_ONLY :
                getObjectsFromDB(User.class);
                break;
            case PERSISTED_NRFRS:
                restServices.getRemoteUser();
                getObjectsFromDB(User.class);
                break;
            default:
                break;
        }
    }

    public static  void getNews(final int whichDataYouNeed){
        switch (whichDataYouNeed){
            case NRFRS:
                restServices.getRemoteNews();
                break;
            case PERSISTED_DATA_ONLY :
                getObjectsFromDB(User.class);
                break;
            case PERSISTED_NRFRS:
                restServices.getRemoteUser();
                getObjectsFromDB(User.class);
                break;
            default:
                break;
        }
    }




    /**
     * Get any object from. The result is post on DataBus
     * @param nededModel an object class who extend RealmObject
     */
    @ReadOnly
    private  static <T extends RealmObject> void getObjectsFromDB(Class<T> nededModel){
        dataBaseService.findAll(nededModel);
    }

}
