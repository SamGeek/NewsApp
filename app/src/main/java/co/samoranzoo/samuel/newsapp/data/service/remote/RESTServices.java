package co.samoranzoo.samuel.newsapp.data.service.remote;

import java.util.List;

import co.samoranzoo.samuel.newsapp.app.App;
import co.samoranzoo.samuel.newsapp.data.models.News;
import co.samoranzoo.samuel.newsapp.data.models.User;
import co.samoranzoo.samuel.newsapp.event.NetworkError;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Shadaï ALI on 28/11/16.
 *
 * @Doc $doc
 */

public class RESTServices {


    public void getRemoteUser(){
        App.getNetworkService().getUser().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                App.getDataBus().post(response.body().get(0));
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                App.getDataBus().post(new NetworkError(NetworkError.UNKNOW,t.getMessage()));
            }
        });
    }

    public void getRemoteNews(){
        App.getNetworkService().getNews().enqueue(new Callback<List<News>>() {
            @Override
            public void onResponse(Call<List<News>> call, Response<List<News>> response) {
                App.getDataBus().post(response.body());
            }

            @Override
            public void onFailure(Call<List<News>> call, Throwable t) {
                App.getDataBus().post(new NetworkError(NetworkError.UNKNOW,t.getMessage()));
            }
        });
    }


}
