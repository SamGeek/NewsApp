package co.samoranzoo.samuel.newsapp.data.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Shadaï ALI on 26/11/16.
 * <p>
 * <b>Model</b> class represents our business data.
 * It must extend @code RealObject for persistence
 * <p>
 * User class by default generated for managed your user information's
 * you can edit it by changing proprieties. Don't forget generating getter
 * and setters for each added propriety
 */

public class User extends RealmObject
/**
 *  user correct parent super if you use other ORM than Realm @link Realm.io
 *  i.e :<code>public class extends SugarRecord{...}</code>  if you Sugar SQLITE ORM
 */

{
    @PrimaryKey
    @SerializedName("userId") //adapt it with the right name returned by you REST API
    private long id;

    @SerializedName("name")
    private String firstName;

    @SerializedName("prename")
    private String lastName;

    @SerializedName("password")
    private String encryptedPassword;

    public User() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }
}
