package co.samoranzoo.samuel.newsapp.event;

import android.util.Log;

import co.samoranzoo.samuel.newsapp.app.App;
import co.samoranzoo.samuel.newsapp.utils.ReadOnly;

import java.util.Locale;

import retrofit2.Response;

/**
 * Created by Shadaï ALI on 25/11/16.
 * <p>
 * <H3>Manage all HTTP RESTFULL call error</H3>
 * <p>For use this manager on network error callback instantiate this
 * Exemple : <code> ErrorEventBus.post(new NetworkError(response.code,
 * response.message));</code>
 * class and send it in network error bus manager  </br>
 * After that it will provides you some features as :
 * <ul>
 * <li>Display automatically error in log when you are in debug mode </li>
 * <li>provides human readable message with getMessage() @link NetworkError#getMessage</li>
 * </ul>
 * </p>
 */

@ReadOnly
public class NetworkError {


    private static final String TAG = "|NetworkError";
    //HTTP error code (value = 0) if no
    // error are detected
    private int code = 0;
    //HTTP error message
    private String message;
    private String debugMessage;

    private static final int HTTP_404 = 404;
    private static final int HTTP_403 = 403;
    private static final int HTTP_500 = 500;
    public static final int UNKNOW = -1003;

    /**
     * Instantiate error manager class with
     * error code <b> if you class constructor you can't
     * see error message in log</b>
     *
     * @param code htpp error code
     */
    private NetworkError(int code) {
        this.code = code;
    }

    /**
     * instantiate error manager class with
     * error code and server msg
     *
     * @param code         htpp error code
     * @param debugMessage http error message
     */
    public NetworkError(int code, String debugMessage) {
        this.code = code;
        this.debugMessage = debugMessage;
        Log.e(TAG, String.format(Locale.FRENCH, "code : %s \n message %s ", code, debugMessage));
    }

    /**
     * getUser human readable message from network error
     *
     * @return String
     */
    public String getMessage() {

        //display error for developers when in debug mode
        switch (code) {
            case HTTP_403:
//                message = App.getContext().getString(R.string.http_not_authorized_msg);
                break;
            case HTTP_404:
//                message = App.getContext().getString(R.string.http_not_found_msg);
                break;
            case HTTP_500:
//                message = App.getContext().getString(R.string.http_server_internal_error);
                break;
            default:
                message =  debugMessage;
                break;
        }
        return message;
    }

    /**
     * getUser  error code
     *
     * @return int
     */
    public int getCode() {
        return code;
    }


    /**
     * lazy network error management
     * instantiate error manager and post it on
     * error event bus
     *
     * @param code error code
     * @param msg  error message
     */
    public static void postError(int code, String msg) {
        App.getNetworkErrorBus().post(new NetworkError(code, msg));
    }

    /**
     * lazy network error management
     * instantiate error manager and post it on
     * error event bus
     *
     * @param response retrofit response
     */
    public static void postError(Response response) {
        if (response == null) {
            Log.e(TAG, "retrofit response that you have post in error bus is null");
            return;
        }
        postError(response.code(), response.message());
        if (null == response.raw()) {
            Log.e(TAG, "retrofit : raw response is null");
            return;
        }
        Log.e(TAG, String.format(Locale.FRENCH, "raw request : %s", response.raw().request().toString()));

        if (null == response.raw().headers()) {
            Log.e(TAG, "retrofit : request  headers is null");
            return;
        }
        Log.e(TAG, String.format(Locale.FRENCH, "retrofit : request headers : %s", response.raw().headers().toString()));
    }
}
