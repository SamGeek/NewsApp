package co.samoranzoo.samuel.newsapp.presenters.news;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import co.samoranzoo.samuel.newsapp.data.models.News;
import co.samoranzoo.samuel.newsapp.data.service.DataManager;
import co.samoranzoo.samuel.newsapp.event.NetworkError;
import co.samoranzoo.samuel.newsapp.presenters.BasePresenter;
import co.samoranzoo.samuel.newsapp.views.news.NewsView;

/**
 * Created by Shadaï ALI on 08/12/16.
 */

public class NewsPresenter extends BasePresenter{

    private NewsView view;

    public NewsPresenter(NewsView view) {
        super();
        this.view = view;
    }

    public void getRemoteNews() {
        view.showProgress();
        DataManager.getNews(DataManager.NRFRS);
    }

    @Subscribe
    public void onListNewsReady(List<News> newsList) {
        view.hideProgress();
        view.showNewsList(newsList);
    }

    @Subscribe
    public void onError(NetworkError error) {
        view.hideProgress();
        view.throwError(error);

    }
}
