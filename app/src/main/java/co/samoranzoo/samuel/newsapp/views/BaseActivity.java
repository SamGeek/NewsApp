package co.samoranzoo.samuel.newsapp.views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import co.samoranzoo.samuel.newsapp.presenters.BasePresenter;
import co.samoranzoo.samuel.newsapp.utils.ReadOnly;


/**
 *
 * Created By Shadaï ALI
 * Base class for every activity. This class provide such base features likes
 * - EventBus management
 * -
 */

public class BaseActivity <T extends BasePresenter> extends AppCompatActivity {

    protected T presenter;

    @Override
    @ReadOnly
    protected void onStart() {
        super.onStart();
        presenter.registerEvent();

     /*   //Subscribe to Data Bus
        EventBus.getDefault().register(this);
        App.getDataBus().register(this);
        App.getNetworkErrorBus().register(this);*/
    }

    @Override
    @ReadOnly
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_base);
    }

    @Override
    @ReadOnly
    protected void onStop() {
        super.onStop();
        presenter.unRegisterEvent();
     /*   //UnSubscribe to Data Bus
        EventBus.getDefault().unregister(this);
        App.getDataBus().unregister(this);
        App.getNetworkErrorBus().unregister(this);*/
    }

}
