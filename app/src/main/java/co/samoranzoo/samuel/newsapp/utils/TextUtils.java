package co.samoranzoo.samuel.newsapp.utils;

import android.util.Log;
import android.widget.EditText;

/**
 * Created by Shadaï ALI on 07/12/16.
 *
 * @Doc $doc
 */

public class TextUtils {

    public static boolean isValidEmail(String email){
        return false;
        //TODO : is  not implemented
    }


    /**
     *
     * @param ed edittext from which you want to retrieve a string
     * @return String <code>EditText.getText().toString().trim()</code> with empty and nullity check
     */
    public static String getString(EditText ed){
        if (null == ed ){
            Log.e("EditText ", "is null");
            return "";
        }
        else if (android.text.TextUtils.isEmpty(ed.getText().toString())){
            Log.w("EditText", "is empty");
            return ed.getText().toString().trim();
        }else return ed.getText().toString().trim();
    }
}
