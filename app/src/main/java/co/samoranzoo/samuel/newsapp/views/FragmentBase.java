package co.samoranzoo.samuel.newsapp.views;

import android.support.v4.app.Fragment;

import org.greenrobot.eventbus.EventBus;

import co.samoranzoo.samuel.newsapp.app.App;

/**
 * Created by Shadaï ALI on 25/11/16.
 * <p>
 * Base class for every fragment. This class provide such base features likes
 * - EventBus management
 */

public class FragmentBase extends Fragment {

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        App.getDataBus().register(this);
        App.getNetworkErrorBus().register(this);
    }


    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        App.getDataBus().unregister(this);
        App.getNetworkErrorBus().unregister(this);
    }
}
