package co.samoranzoo.samuel.newsapp.data.service.local;


import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

/**
 * Created by Shadaï ALI on 25/11/16.
 * It provides tools for manage database migration
 * @Doc $doc
 */

public class MyRealmMigration implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {

        RealmSchema schema = realm.getSchema();
        /**
         * if your model change
         * make a migation like this
         *
         * Example:
         * public Person extends RealmObject {
         *     private String name;
         *     private int age;
         *     * getters and setters left out for brevity
         * }
         if (oldVersion == 0) {
         schema.create("Person")
         .addField("name", String.class)
         .addField("age", int.class);
         oldVersion++;
         }

         * Migrate to version 2: Add a primary key + object references
         * Example:
         * public Person extends RealmObject {
         *     private String name;
         *     @PrimaryKey
         *     private int age;
         *     private Dog favoriteDog;
         *     private RealmList<Dog> dogs;
         *      getters and setters left out for brevity
         * }
         if (oldVersion == 1) {
         schema.getUser("Person")
         .addField("id", long.class, FieldAttribute.PRIMARY_KEY)
         .addRealmObjectField("favoriteDog", schema.getUser("Dog"))
         .addRealmListField("dogs", schema.getUser("Dog"));
         oldVersion++;
         */
    }
}
