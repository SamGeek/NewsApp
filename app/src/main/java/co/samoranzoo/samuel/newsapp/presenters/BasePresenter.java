package co.samoranzoo.samuel.newsapp.presenters;

import co.samoranzoo.samuel.newsapp.app.App;
import co.samoranzoo.samuel.newsapp.utils.ReadOnly;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Shadaï ALI on 08/12/16.
 *
 * @Doc $doc
 */

@ReadOnly
public class BasePresenter {
    //TODO manage on start and on destroy life cycle


  /*  public BasePresenter() {

    }*/

    public void registerEvent(){
        EventBus.getDefault().register(this);
        App.getDataBus().register(this);
        App.getNetworkErrorBus().register(this);
    }


    public void unRegisterEvent(){
        EventBus.getDefault().unregister(this);
        App.getDataBus().unregister(this);
        App.getNetworkErrorBus().unregister(this);
    }
}
