package co.samoranzoo.samuel.newsapp.event;

/**
 * Created by Shadaï ALI on 08/12/16.
 *
 * @Doc $doc
 */

public class Error {

    private DbMessage dbMessage ;
    private NetworkError networkError;

    public Error(DbMessage dbMessage) {
        this.dbMessage = dbMessage;
    }

    public Error(NetworkError networkError) {
        this.networkError = networkError;
    }

    public Error(DbMessage dbMessage, NetworkError networkError) {
        this.dbMessage = dbMessage;
        this.networkError = networkError;
    }

    public DbMessage getDbMessage() {
        return dbMessage;
    }

    public NetworkError getNetworkError() {
        return networkError;
    }
}
