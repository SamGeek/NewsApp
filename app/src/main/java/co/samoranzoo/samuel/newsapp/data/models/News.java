package co.samoranzoo.samuel.newsapp.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by samuel on 08/06/17.
 */

public class News {

    @SerializedName("intitule")
    String intitule;
    @SerializedName("content")
    String contenu;
    @SerializedName("createdAt")
    String datePublication;
    @SerializedName("imageLink")
    String imageUrl;

    public News(String intitule, String contenu, String datePublication, String imageUrl) {
        this.intitule = intitule;
        this.contenu = contenu;
        this.datePublication = datePublication;
        this.imageUrl = imageUrl;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public String getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(String datePublication) {
        this.datePublication = datePublication;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
