package co.samoranzoo.samuel.newsapp.views;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import co.samoranzoo.samuel.newsapp.R;
import co.samoranzoo.samuel.newsapp.data.models.News;

/**
 * Created by samuel on 17/01/17.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder> {

    private List<News> listComptes;
    public AppCompatActivity mActivity;


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView nom, numeroCompte;
        private Context context;


        public MyViewHolder(Context context, View view) {
            super(view);
            nom = (TextView) view.findViewById(R.id.intitule);
            numeroCompte = (TextView) view.findViewById(R.id.date_publication);
            view.findViewById(R.id.cardAccount).setOnClickListener(this);
            this.context = context;
        }

        @Override
        public void onClick(View view) {

        mActivity.startActivity(new Intent(mActivity,DetailsActivity.class));
//            mActivity.finish();
        }
    }

    public NewsAdapter(List<News> newsList, AppCompatActivity mActivity) {
        this.listComptes = newsList;
        this.mActivity = mActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_row, parent, false);

        return new MyViewHolder(mActivity,itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

//        Compte compte =  listComptes.get(position);
//        final String[] civilites =mActivity.getResources().getStringArray(R.array.spinner_items);
//        int indexSexeClient = Integer.parseInt(compte.getClient().getSexe());
//        holder.nom.setText(civilites[indexSexeClient] + " "+compte.getClient().getFullUpperName());
//        holder.numeroCompte.setText(mActivity.getString(R.string.account_number_intro) +TextUtils.spacer(compte.getNumeroCompte(),1));

    }



    @Override
    public int getItemCount() {
        return listComptes.size();
    }

}
